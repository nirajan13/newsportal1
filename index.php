<!DOCTYPE html>
<html lang="ne">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Acejourn </title>
      <link rel="stylesheet" type="text/css" href="css/owl-carousel.css"/>
      <link rel="stylesheet" type="text/css" href="css/owl-theme.css"/>
      <link rel="stylesheet" type="text/css" href="css/normalize.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-4.min.css">
      <link rel="stylesheet" type="text/css" href="css/animate.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   </head>
   <body>
   	
      <div class="header">
         <div class="primary-background">
            <div class="container">
               <div class="header-top">
                  <div class="top-header-left">
                     १ चैत २०७८, मंगलवार
                  </div>
                  <div class="top-header-right">
                     <a href="category-page.html">हाम्रोबारे</a> /
                     <a href="category-page.html">सम्पर्क</a> /
                     <a href="category-page.html">रोजगार</a>
                  </div>
               </div>
            </div>
         </div>
         <div class="header-logo">
            <div class="logo">
                  <img src="images/acejourn.jpg" alt="">
            </div>
         </div>
      </div>
      <section class="navbar primary-background">
         <div class="container">
            <ul class="navbar">
               <li class="nav-logo logo show-tab"><a href="category-page.html"><img src="images/acejourn.jpg" alt=""></a></li>
               <li><a href="category-page.html" class="active">कथासार</a></li>
               <li><a href="category-page.html">खेलकुद</a></li>
               <li><a href="category-page.html">राजनीती</a></li>
               <li class="section-dropdown-menu">
                  <a href="category-page.html">मनोरन्जन</a>
                  <ul class="section-dropdown-menu-child primary-background">
                     <li><a href="category-page.html">चलचित्र</a></li>
                     <li><a href="category-page.html">टेलि-शृङ्खला</a></li>
                  </ul>
               </li>
               <li class="dropdown-megamenu">
                  <button class="dropbtn">समाचार 
                  <i class="fa fa-caret-down"></i>
                  </button>
                  <div class="dropdown-content megamenu">
                     <div class="header">
                        <h2>Mega Menu</h2>
                     </div>
                     <div class="row">
                        <div class="col-4 primary-background">
                           <h5 class="section-headline">Category</h5>
                           <a href="#">Link 1</a>
                           <a href="#">Link 2</a>
                           <a href="#">Link 3</a>
                           <a href="#">Link 4</a>
                        </div>
                        <div class="col-4 primary-background">
                           <h5 class="section-headline">Category</h5>
                           <a href="#">Link 1</a>
                           <a href="#">Link 2</a>
                           <a href="#">Link 3</a>
                           <a href="#">Link 4</a>
                        </div>
                        <div class="col-4 primary-background">
                           <h5 class="section-headline">Category</h5>
                           <a href="#">Link 1</a>
                           <a href="#">Link 2</a>
                           <a href="#">Link 3</a>
                           <a href="#">Link 4</a>
                        </div>
                     </div>
                  </div>
               </li>
               <li><a href="category-page.html">ब्यापार</a></li>
               <li><a href="category-page.html">जीवनशैली</a></li>
            </ul>
            <div class="search-box">
               <i class="fa fa-search"></i>
            </div>
         </div>
      </section>
      
      <section class="container">
         <div class="ads">
            <img src="ads/nepatop.gif" alt="">
         </div>
         <div class="featured-news-container">
            <h1 class="news-headline"> अर्थतन्त्र भयावह अवस्थामा छ, सरकारले श्वेतपत्र जारी गरोस् : ओली </h1>
            <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
            <div class="news-info primary-color"><span></span><span> -Nirajan Dahal </span></div>
            <p class="news-content">
               २९ फागुन, काठमाडौं । नेकपा एमालेका अध्यक्ष केपी शर्मा ओलीले मुलुकको अर्थतन्त्र भयवह अवस्थामा पुगेको दावी गरेका छन् । पार्टीको उद्योग, वाणिज्य तथा आपूर्ति विभागले गरेको अर्थतन्त्रको वर्तमान अवस्थाबारे अन्र्तकि्रयामा बोल्दै ओलीले गलत प्रक्रियाबाट बनेको सरकारका कारण अर्थतन्त्र पनि गलत बाटोतर्फ मोडिएको दावी गरे ।
            </p>
            <p class="news-content">	
               उनले मुलुृकको वर्तमान अर्थतन्त्रको अवस्थाबारे श्वेतपत्र जारी गर्न पनि अर्थमन्त्रीलाई चुनौती दिए । सरकार गठबन्धन जोगाउन मात्रै लागेको भन्दै उनले प्रधानमन्त्री र मन्त्रीहरुको कठोर आलोचना गरे । प्रधानमन्त्री पद आकस्मिक रुपमा पाएकाले त्यसलाई जोगाउन नै प्रधानमन्त्री लागेको बताए ।
            </p>
         </div>
      </section>
      <div class="container">
         <div class="ads">
            <img src="ads/nepatop.gif" alt="">
         </div>
      </div>
      <section class="highlighted-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <h1 class="section-headline">ताजा खबर</h1>
               </div>
               <div class="news-list-section row">
                  <div class="media m-b-15 col-sm-4">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
                  <div class="media m-b-15 col-sm-4">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
                  <div class="media m-b-15 col-sm-4">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
                  <div class="media m-b-15 col-sm-4">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
                  <div class="media m-b-15 col-sm-4">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
                  <div class="media m-b-15 col-sm-4">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
                  <div class="media m-b-15 col-sm-4">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
                  <div class="media m-b-15 col-sm-4">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
                  <div class="media m-b-15 col-sm-4">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- <section class="headlines main-container">
         <div class="clearfix m-b-15">
          <div class="col-50 p-r-15">
          	<div class="news-slider">
             <div class="text-over-image-div">
             	<img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
             	<h2 class="text-headline"><a href="single-page.html">KP OLI1</a></h2>
             </div>
             <div class="text-over-image-div">
             	<img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
             	<h2 class="text-headline"><a href="single-page.html">KP OLI2</a></h2>
             </div>
             <div class="text-over-image-div">
             	<img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
             	<h2 class="text-headline"><a href="single-page.html">KP OLI3</a></h2>
             </div>
         </div>
          </div>
         
          <div class="col-25 p-r-15">
          	<div class="media m-b-15">
          		<div class="media-image p-r-15">
          			<img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
          		</div>
          		<div class="media-text">
          			<h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
          			<div class="news-info primary-color"><span> -Nirajan Dahal </span></div>
          		</div>
          	</div>
         
          	<div class="media m-b-15">
          		<div class="media-image p-r-15">
          			<img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
          		</div>
          		<div class="media-text">
          			<h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
          			<div class="news-info primary-color"><span> -Nirajan Dahal </span></div>
          		</div>
          	</div>
         
          	<div class="media m-b-15">
          		<div class="media-image p-r-15">
          			<img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
          		</div>
          		<div class="media-text">
          			<h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
          			<div class="news-info primary-color"><span> -Nirajan Dahal </span></div>
          		</div>
          	</div>	
          </div>
         
          <div class="col-25">
          	<div class="media m-b-15">
          		<div class="media-image p-r-15">
          			<img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
          		</div>
          		<div class="media-text">
          			<h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
          			<div class="news-info primary-color"><span> -Nirajan Dahal </span></div>
          		</div>
          	</div>
         
          	<div class="media m-b-15">
          		<div class="media-image p-r-15">
          			<img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
          		</div>
          		<div class="media-text">
          			<h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
          			<div class="news-info primary-color"><span> -Nirajan Dahal </span></div>
          		</div>
          	</div>
         
          	<div class="media m-b-15">
          		<div class="media-image p-r-15">
          			<img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
          		</div>
          		<div class="media-text">
          			<h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
          			<div class="news-info primary-color"><span> -Nirajan Dahal </span></div>
          		</div>
          	</div>	
          </div>
         </div>
         
         <div class="clearfix">
         	<div class="cards-section-1 news-cards col-66">
         		<div class="col-50">
         			<div class="text-over-image-div m-r-15">
             	<img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
             	<h2 class="text-headline"><a href="single-page.html">KP OLI1</a></h2>
             </div>
         		</div>
         		<div class="col-50">
         			<div class="media m-b-15">
           		<div class="media-image p-r-15">
           			<img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
           		</div>
           		<div class="media-text">
           			<h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
           			<div class="news-info primary-color"><span> -Nirajan Dahal </span></div>
           		</div>
           	</div>
         
           	<div class="media m-b-15">
           		<div class="media-image p-r-15">
           			<img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
           		</div>
           		<div class="media-text">
           			<h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
           			<div class="news-info primary-color"><span> -Nirajan Dahal </span></div>
           		</div>
           	</div>
         
           	<div class="media m-b-15">
           		<div class="media-image p-r-15">
           			<img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
           		</div>
           		<div class="media-text">
           			<h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
           			<div class="news-info primary-color"><span> -Nirajan Dahal </span></div>
           		</div>
           	</div>
         		</div>
         	</div>
         	<div class="col-33">
         		<div class="cards-section-1 news-cards col-66">
         		<div class="col-50">
         			<div class="text-over-image-div m-r-15">
             	<img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
             	<h2 class="text-headline"><a href="single-page.html">KP OLI1</a></h2>
             </div>
         		</div>
         		<div class="col-50">
         			<div class="media m-b-15">
           		<div class="media-image p-r-15">
           			<img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
           		</div>
           		<div class="media-text">
           			<h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
           			<div class="news-info primary-color"><span> -Nirajan Dahal </span></div>
           		</div>
           	</div>
         
           	<div class="media m-b-15">
           		<div class="media-image p-r-15">
           			<img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
           		</div>
           		<div class="media-text">
           			<h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
           			<div class="news-info primary-color"><span> -Nirajan Dahal </span></div>
           		</div>
           	</div>
         
           	<div class="media m-b-15">
           		<div class="media-image p-r-15">
           			<img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
           		</div>
           		<div class="media-text">
           			<h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
           			<div class="news-info primary-color"><span> -Nirajan Dahal </span></div>
           		</div>
           	</div>
         		</div>
         	</div>
         	</div>
         </div>
         </section> -->
      <section class="headlines">
         <div class="container">
            <div class="ads">
               <img src="ads/nepatop.gif" alt="">
            </div>
            <div class="news-cards">
               <div class="row">
                  <div class="col-12">
                     <h1 class="section-headline">सम्पादक ले रुचाएको </h1>
                  </div>
                  <div class="col-md-9">
                     <div class="news-card-1 row">
                        <div class="col-md-8">
                           <div class="news-slider">
                              <div class="text-over-image-div">
                                 <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                                 <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                              </div>
                              <div class="text-over-image-div">
                                 <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                                 <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                              </div>
                              <div class="text-over-image-div">
                                 <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                                 <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="media-news">
                              <div class="media m-b-15">
                                 <div class="media-image p-r-15">
                                    <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                                 </div>
                                 <div class="media-text">
                                    <h2 class="text-headline"><a href="single-page.html">अर्थतन्त्र भयावह अवस्थामा छ, सरकारले श्वेतपत्र जारी गरोस् : ओली</a></h2>
                                    <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                                 </div>
                              </div>
                              <div class="media m-b-15">
                                 <div class="media-image p-r-15">
                                    <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                                 </div>
                                 <div class="media-text">
                                    <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                                    <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                                 </div>
                              </div>
                              <div class="media m-b-15">
                                 <div class="media-image p-r-15">
                                    <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                                 </div>
                                 <div class="media-text">
                                    <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                                    <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="ads">
                        <img src="ads/maruti-cement.gif" alt="">
                     </div>
                  </div>
               </div>
            </div>
            <div class="ads">
               <img src="ads/nepatop.gif" alt="">
            </div>
            <div class="news-cards">
               <div class="row">
                  <div class="col-12">
                     <h1 class="section-headline">कथासार </h1>
                  </div>
                  <div class="col-md-4">
                     <div class="text-over-image-div">
                        <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                        <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                     </div>
                     <div class="news-info">
                        <span>2 minutes ago</span>
                        <span>Entertainment</span>
                     </div>
                     <p class="news-content">
                        २९ फागुन, काठमाडौं । नेकपा एमालेका अध्यक्ष केपी शर्मा ओलीले मुलुकको अर्थतन्त्र भयवह अवस्थामा पुगेको दावी गरेका छन् । 
                     </p>
                  </div>
                  <div class="col-md-4">
                     <div class="text-over-image-div">
                        <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                        <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                     </div>
                     <div class="news-info">
                        <span>2 minutes ago</span>
                        <span>Entertainment</span>
                     </div>
                     <p class="news-content">
                        २९ फागुन, काठमाडौं । नेकपा एमालेका अध्यक्ष केपी शर्मा ओलीले मुलुकको अर्थतन्त्र भयवह अवस्थामा पुगेको दावी गरेका छन् । 
                     </p>
                  </div>
                  <div class="col-md-4">
                     <div class="text-over-image-div">
                        <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                        <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                     </div>
                     <div class="news-info">
                        <span>2 minutes ago</span>
                        <span>Entertainment</span>
                     </div>
                     <p class="news-content">
                        २९ फागुन, काठमाडौं । नेकपा एमालेका अध्यक्ष केपी शर्मा ओलीले मुलुकको अर्थतन्त्र भयवह अवस्थामा पुगेको दावी गरेका छन् । 
                     </p>
                  </div>
               </div>
            </div>
            <div class="ads">
               <img src="ads/nepatop.gif" alt="">
            </div>
            <div class="news-cards">
               <div class="row">
                  <div class="col-md-8">
                     <div class="row">
                        <div class="col-12">
                           <h1 class="section-headline">राजनीती </h1>
                        </div>
                        <div class="col-md-12">
                           <div class="text-over-image-div m-b-15">
                              <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                              <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="media-vertical">
                              <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                              <div class="news-info">
                                 <span>2 minutes ago</span>
                              </div>
                              <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="media-vertical">
                              <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                              <div class="news-info">
                                 <span>2 minutes ago</span>
                              </div>
                              <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="media-vertical">
                              <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                              <div class="news-info">
                                 <span>2 minutes ago</span>
                              </div>
                              <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="media-vertical">
                              <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                              <div class="news-info">
                                 <span>2 minutes ago</span>
                              </div>
                              <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="media-vertical">
                              <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                              <div class="news-info">
                                 <span>2 minutes ago</span>
                              </div>
                              <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="media-vertical">
                              <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                              <div class="news-info">
                                 <span>2 minutes ago</span>
                              </div>
                              <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="media-vertical">
                              <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                              <div class="news-info">
                                 <span>2 minutes ago</span>
                              </div>
                              <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="media-vertical">
                              <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                              <div class="news-info">
                                 <span>2 minutes ago</span>
                              </div>
                              <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                           </div>
                        </div>
                     </div>
                     <div class="ads">
                        <img src="ads/fresh-breath.gif" alt="">
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div>
                        <div class="col-12">
                           <h1 class="section-headline">बिचार</h1>
                        </div>
                        <div class="media m-b-15">
                           <div class="media-image p-r-15">
                              <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                           </div>
                           <div class="media-text">
                              <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                              <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                           </div>
                        </div>
                        <div class="media m-b-15">
                           <div class="media-image p-r-15">
                              <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                           </div>
                           <div class="media-text">
                              <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                              <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                           </div>
                        </div>
                        <div class="media m-b-15">
                           <div class="media-image p-r-15">
                              <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                           </div>
                           <div class="media-text">
                              <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                              <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                           </div>
                        </div>
                        <div class="media m-b-15">
                           <div class="media-image p-r-15">
                              <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                           </div>
                           <div class="media-text">
                              <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                              <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                           </div>
                        </div>
                     </div>
                     <div class="ads">
                        <img src="ads/maruti-cement.gif" alt="">
                     </div>
                  </div>
               </div>
            </div>
            <div class="ads">
               <img src="ads/nepatop.gif" alt="">
            </div>
         </div>
      </section>
      <section>
         <div class="container">
            <div class="row">
               <div class="col-md-8">
                  <div class="row">
                     <div class="col-12">
                        <h1 class="section-headline">समाचार </h1>
                     </div>

                     <div class="col-md-12">
                     	<div class="news-card-1 row">
                        <div class="col-md-8">
                           <div class="news-slider">
                              <div class="text-over-image-div">
                                 <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                                 <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                              </div>
                              <div class="text-over-image-div">
                                 <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                                 <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                              </div>
                              <div class="text-over-image-div">
                                 <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                                 <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="media-news">
                              <div class="media m-b-15">
                                 <div class="media-image p-r-15">
                                    <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                                 </div>
                                 <div class="media-text">
                                    <h2 class="text-headline"><a href="single-page.html">अर्थतन्त्र भयावह अवस्थामा छ, सरकारले श्वेतपत्र जारी गरोस् : ओली</a></h2>
                                    <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                                 </div>
                              </div>
                              <div class="media m-b-15">
                                 <div class="media-image p-r-15">
                                    <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                                 </div>
                                 <div class="media-text">
                                    <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                                    <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                                 </div>
                              </div>
                              <div class="media m-b-15">
                                 <div class="media-image p-r-15">
                                    <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                                 </div>
                                 <div class="media-text">
                                    <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                                    <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </div>

                     <div class="col-md-4">
                        <div class="media-vertical">
                           <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                           <div class="news-info">
                              <span>2 minutes ago</span>
                           </div>
                           <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="media-vertical">
                           <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                           <div class="news-info">
                              <span>2 minutes ago</span>
                           </div>
                           <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="media-vertical">
                           <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                           <div class="news-info">
                              <span>2 minutes ago</span>
                           </div>
                           <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="media-vertical">
                           <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                           <div class="news-info">
                              <span>2 minutes ago</span>
                           </div>
                           <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="media-vertical">
                           <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                           <div class="news-info">
                              <span>2 minutes ago</span>
                           </div>
                           <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="media-vertical">
                           <img src="https://sanskritinepal.com/wp-content/uploads/2021/05/1587379442Oli.jpg" alt="">
                           <div class="news-info">
                              <span>2 minutes ago</span>
                           </div>
                           <h2 class="text-headline"><a href="single-page.html">८ महिनामा विकास बजेट खर्च ७७ अर्ब मात्र </a></h2>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="col-12">
                     <h1 class="section-headline">लोकप्रिय </h1>
                  </div>
                  <div class="media m-b-15">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
                  <div class="media m-b-15">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
                  <div class="media m-b-15">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
                  <div class="media m-b-15">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">बालबालिकालाई प्रश्न सोध्न सिकाउनुपर्छ</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <div class="container">
      	<div class="ads">
	         <img src="ads/fresh-breath.gif" alt="">
	      </div>
	      <div class="ads">
	         <img src="ads/nepatop.gif" alt="">
	      </div>
      </div>
      
      <section class="highlighted-section">
         <div class="container">
            <div class="row">
               <div class="col-md-4">
                  <h3 class="section-headline">जलवायु</h3>
                  <div class="text-over-image-div m-b-15">
                     <img src="https://bg.annapurnapost.com/uploads/media/climate-change_20220316072638.jpg" alt="">
                     <h2 class="text-headline"><a href="single-page.html">जलवायु परिवर्तनका आयाम </a></h2>
                  </div>
                  <div class="media m-b-15">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">अर्थतन्त्र भयावह अवस्थामा छ, सरकारले श्वेतपत्र जारी गरोस् : ओली</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
                  <div class="media m-b-15">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">अर्थतन्त्र भयावह अवस्थामा छ, सरकारले श्वेतपत्र जारी गरोस् : ओली</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <h3 class="section-headline">होली बिशेश</h3>
                  <div class="text-over-image-div m-b-15">
                     <img src="https://resize.indiatvnews.com/en/resize/newbucket/1200_-/2020/03/holi-1583485335.jpg" alt="">
                     <h2 class="text-headline"><a href="single-page.html">प्रकृतिको रंग, होलीको उमंग </a></h2>
                  </div>
                  <div class="media m-b-15">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">अर्थतन्त्र भयावह अवस्थामा छ, सरकारले श्वेतपत्र जारी गरोस् : ओली</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
                  <div class="media m-b-15">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">अर्थतन्त्र भयावह अवस्थामा छ, सरकारले श्वेतपत्र जारी गरोस् : ओली</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <h3 class="section-headline">सिने लहर</h3>
                  <div class="text-over-image-div m-b-15">
                     <img src="https://www.pinkvilla.com/imageresize/ishq_mein_hoon.jpg?width=752&format=webp&t=pvorg" alt="">
                     <h2 class="text-headline"><a href="single-page.html">‘राधे श्याम’को कमजोर सुरुवात</a></h2>
                  </div>
                  <div class="media m-b-15">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">अर्थतन्त्र भयावह अवस्थामा छ, सरकारले श्वेतपत्र जारी गरोस् : ओली</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
                  <div class="media m-b-15">
                     <div class="media-image p-r-15">
                        <img src="https://www.onlinekhabar.com/wp-content/uploads/2021/04/Baburam-bhattarai-768x466.jpg" alt="">
                     </div>
                     <div class="media-text">
                        <h2 class="text-headline"><a href="single-page.html">अर्थतन्त्र भयावह अवस्थामा छ, सरकारले श्वेतपत्र जारी गरोस् : ओली</a></h2>
                        <div class="news-info primary-color"><span> - 4 Days Ago </span></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <script src="js/jquery-3.3.1.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap-4.min.js"></script>
      <script type="text/javascript" src="js/owl-carousel.js"></script>
      <script>
         $('.news-slider').owlCarousel({
         items:1,
         loop:true,
         margin:10,
         merge:true,
         responsive:{
             678:{
                 mergeFit:true
             },
             1000:{
                 mergeFit:false
             }
         }
         });
      </script>

      <script>
      	document.getElementById("hide").onclick = function() {
		    document.getElementById("register").style.display = "none";
		}
		 
		document.getElementById("show").onclick = function() {
		    document.getElementById("register").style.display = "block";
		}
      </script>
   </body>
</html>